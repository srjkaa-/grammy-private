export class Slider {

    el: any;
    container: any;
    items: any;
    left: any;
    right;
    dots;
    viewportWidth: number;
    slideIndex: number;
    fakeSlides: number;
    currentPage: string;

  constructor(id: any) {
    this.el = id;
    this.container = this.el.querySelector('div.slider-container');
    this.items = this.el.querySelectorAll('.slider-item');
    this.left;
    this.right;
    this.dots;
    this.viewportWidth = this.el.offsetWidth;
    this.slideIndex = 0;
    this.fakeSlides = 4;
    this.currentPage = location.pathname;
    this.init();
  }

  init() { 
    this.computeItemWidth();
    this.createElements();
    this.initEvents();
    this.slide(0)
    this.autoslide.apply(this);
  }

  computeItemWidth() {

    if (this.viewportWidth !== this.el.offsetWidth) {
        this.viewportWidth = this.el.offsetWidth;
    }
    this.items.forEach(item => {
      item.style.width = this.viewportWidth + 'px';
    });
  }

  createElements() {
      this.createArrows();
      this.createDots();
      this.appendStyles();
  }

  createArrows() {
      const arrows = `<a (click)="funcNameLeft()" href="" class="slider-left" data-controls="left"></a><a (click)="funcNameRight()" href="" class="slider-right" data-controls="right"></a>`;
      this.el.innerHTML += arrows;
      this.left = this.el.querySelector('a.slider-left');
      this.right = this.el.querySelector('a.slider-right');
  }

  createDots() {
      const dotsContainer = `<div class="slider-dots"></div>`;
      this.el.innerHTML += dotsContainer;
      const dotsContainerInDom = this.el.querySelector('div.slider-dots');
      for (let i = 0; i < (this.items.length - this.fakeSlides); i++) {
          dotsContainerInDom.innerHTML += `<a href="" class="dot"></a>`;
      }
      this.dots = this.el.querySelectorAll('a.dot');
      this.dots[0].classList.add('active');
  }

  appendStyles() {
      const styles = `<style id="common-slider-style">
      .slider-item {
          height: 100% !important
      }
      .slider-item .item-content {
          height: 85% !important; 
      }

      .slider-left, .slider-right {
        position: absolute;
        width: 50px;
        height: 50px;
        display: flex;
        justify-content: center;
        align-items: center;
        background-position: center center;
        top: calc(50% - 25px);
        background-repeat: no-repeat;
        filter: drop-shadow(0px 0px 3px #000);
        transition: .4s ease-in-out;
        opacity: .15;
      }
      .slider-left:hover, .slider-right:hover {
        opacity: 1;
        filter: drop-shadow(0px 0px 3px green);
      }
      .slider-left {
        background-image: url('../../../assets/images/slider-arrows/left-arrow.png');
        left: 0px;
      }
      .slider-right {
        background-image: url('../../../assets/images/slider-arrows/right-arrow.png');
        right: 0px;
      }
      
      .slider-dots {
        position: absolute;
        width: 150px;
        height: 20px;
        bottom: 60px;
        left: calc(50% - 75px);
        display: flex;
        justify-content: space-between;
        z-index: 999;
        opacity: .3;
      }
      .slider-dots .dot {
        width: 20px;
        height: 20px;
        border-radius: 10px;
        border: 1px solid rgba(255, 255, 255, 0.7);
        background-color: transparent;
        transition: .5s ease-in-out;
      }
      .slider-dots .dot.active {
        background-color: rgba(255, 255, 255, .6);
      }
      </style>`;

      if (document.getElementById('common-slider-style') !== null) {
        document.getElementById('common-slider-style').remove();
      }
      this.el.insertAdjacentHTML('afterBegin', styles);
  };

  initEvents() {
    let that = this;
    this.el.addEventListener('click', e => {
      e.preventDefault();

      let target = e.target;

      if (target.getAttribute('data-controls') === 'left') {
        that.slide(that.slideIndex - 1);
      } else if (target.getAttribute('data-controls') === 'right') {
        that.slide(that.slideIndex + 1);
      } else if (target.classList.contains('dot')) {
        that.activateDot(that.dots, target);
        that.slide(that.slideIndex);
      }
    });

    this.handleTouchEvents(this.el, this.container);

    /**
     * Scrolling slides by mousewheel.
     * It works well, but looks like shit.
     * Uncomment if you think that you are going to die
     * and this is the only thing that is (probably) going to rescue you.
     */
    // this.el.addEventListener('wheel', e => {
    //     e.preventDefault();
    //     e.deltaY > 0 ? this.slide(this.slideIndex + 1) : this.slide(this.slideIndex - 1);
    //   });

    window.addEventListener('resize', () => this.resizeWatcher(), false);
  }

  resizeWatcher() {
      const resizeStyle = `.slider-item {
        width: ${this.el.offsetWidth}px !important;
      }`;

      const resizeStyleSelector = `<style id="resize-style-2">
        ${resizeStyle}
      </style>`;

      if (document.getElementById('resize-style-2') !== null) {
        document.getElementById('resize-style-2').innerHTML = resizeStyle;
      } else {
        this.el.insertAdjacentHTML('afterBegin', resizeStyleSelector);
      }

      this.viewportWidth = this.el.offsetWidth;

      this.slide(this.slideIndex);
  }

  handleTouchEvents(slider, sliderContainer) {
    let ondrag = false;
    let touchstartPoint = 0;
    let dragged = 0;

    slider.addEventListener('touchstart', e => {
      ondrag = true;
      touchstartPoint = e.targetTouches[0].pageX;
    });

    slider.addEventListener('touchmove', e => {
      dragged += (e.targetTouches[0].pageX - touchstartPoint);
      sliderContainer.style.transform += `translateX(${dragged}px)`;
      touchstartPoint = e.targetTouches[0].pageX;
    });

    slider.addEventListener('touchend', e => {      
      if (dragged >= 60) {
        this.slide(this.slideIndex - 1);
      } else if (dragged <= -60) {
        this.slide(this.slideIndex + 1);
      } else {
        // this.slide(0);
      }
      dragged = 0;
      ondrag = false;
    });
  }

  translateStyle(delta: number) {
      return `<style id="translate-2">
      .slider-container {
        -webkit-transform: translateX(${delta}px) !important;
        transform: translateX(${delta}px) !important;  
      }
      </style>`;
  }

  translateStyleInnerHTML(delta: number) {
      return `
      .slider-container {
        -webkit-transform: translateX(${delta}px) !important;
        transform: translateX(${delta}px) !important;  
      }`
  }

  slide(index: number) {
    if (index >= (this.items.length - this.fakeSlides) || index < 0) {
        this.jumpTo(index);
    } else {
        let delta = this.viewportWidth * (index + (this.fakeSlides / 2)) * -1;
        if (document.getElementById('translate-2') !== null) {
            document.getElementById('translate-2').innerHTML = this.translateStyleInnerHTML(delta);
        } else {
        this.el.insertAdjacentHTML('afterBegin', this.translateStyle(delta));
        }
        this.slideIndex = index;
        this.activateDot(this.dots, this.dots[index]);
    }    
  }

  jumpTo(i: number) {
    let delta = this.viewportWidth * (i + (this.fakeSlides / 2)) * -1;
    if (document.getElementById('translate-2') !== null) {
        document.getElementById('translate-2').innerHTML = this.translateStyleInnerHTML(delta);
    } else {
        this.el.insertAdjacentHTML('afterBegin', this.translateStyle(delta));
    }    

    if (i < 0) {
        setTimeout(() => {
            this.disableTransitions()
            this.slide(this.items.length - this.fakeSlides - 1);
            setTimeout(() => {
            document.getElementById('disable-transitions-2').remove();
            }, 16);
        }, 500);
    } else if (i > this.items.length - this.fakeSlides - 1) {
        setTimeout(() => {
            this.disableTransitions();
            this.slide(0);
            setTimeout(() => {
                document.getElementById('disable-transitions-2').remove();
            }, 16);        
        }, 500);
    }
  }

  disableTransitions() {
      const style = `<style id="disable-transitions-2">
      .slider-container {
          -webkit-transition: none !important;
          -moz-transition: none !important;
          -o-transition: none !important;
          transition: none !important;
      }
      </style>`
      this.el.insertAdjacentHTML('afterBegin', style);
  }

  activateDot(dots: any, target: any) {
    dots.forEach(dot => dot.classList.remove('active'));
    target.classList.add('active');
    dots.forEach((dot, i) => {
      if (dot.classList.contains('active')) {
        this.slideIndex = i;
      }
    });
  }

  autoslide() {
    let as = setTimeout(() => {
        if (location.pathname !== this.currentPage) {
            clearTimeout(as);
        }
        this.slide(this.slideIndex + 1);
        if (location.pathname === this.currentPage) {
            setTimeout(this.autoslide.apply(this), 7000);
        }
    }, 7000);
  }
}