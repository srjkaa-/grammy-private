import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuService } from './../../../services/menu.service';
import Scrollbar from 'smooth-scrollbar';

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.css', 'menu-item.media.component.css']
})
export class MenuItemComponent implements OnInit, AfterViewInit {

  listOfDishes: any = [
    {
      name: 'стейк с овощами гриль',
      ingredients: 'телятина, картофель, морковь, лук, соус песто, соус соевый',
      description: `Давно выяснено, что при оценке дизайна и композиции читаемый 
        текст мешает сосредоточиться. Используют потому, что тот обеспечивает 
        более или менее стандартное заполнение шаблона`,
      img: '../../assets/images/dish1.png',
    },
    {
      name: 'стейк с овощами гриль',
      ingredients: 'телятина, картофель, морковь, лук, соус песто, соус соевый',
      description: `Давно выяснено, что при оценке дизайна и композиции читаемый 
        текст мешает сосредоточиться. Используют потому, что тот обеспечивает 
        более или менее стандартное заполнение шаблона`,
      img: '../../assets/images/dish2.png'
    },
    {
      name: 'стейк с овощами гриль',
      ingredients: 'телятина, картофель, морковь, лук, соус песто, соус соевый',
      description: `Давно выяснено, что при оценке дизайна и композиции читаемый 
        текст мешает сосредоточиться. Используют потому, что тот обеспечивает 
        более или менее стандартное заполнение шаблона`,
      img: '../../assets/images/dish3.png'
    },
    {
      name: 'стейк с овощами гриль',
      ingredients: 'телятина, картофель, морковь, лук, соус песто, соус соевый',
      description: `Давно выяснено, что при оценке дизайна и композиции читаемый 
        текст мешает сосредоточиться. Используют потому, что тот обеспечивает 
        более или менее стандартное заполнение шаблона`,
      img: '../../assets/images/dish2.png'
    },
    {
      name: 'стейк с овощами гриль',
      ingredients: 'телятина, картофель, морковь, лук, соус песто, соус соевый',
      description: `Давно выяснено, что при оценке дизайна и композиции читаемый 
        текст мешает сосредоточиться. Используют потому, что тот обеспечивает 
        более или менее стандартное заполнение шаблона`,
      img: '../../assets/images/dish3.png'
    },
    {
      name: 'стейк с овощами гриль',
      ingredients: 'телятина, картофель, морковь, лук, соус песто, соус соевый',
      description: `Давно выяснено, что при оценке дизайна и композиции читаемый 
        текст мешает сосредоточиться. Используют потому, что тот обеспечивает 
        более или менее стандартное заполнение шаблона`,
      img: '../../assets/images/dish1.png'
    }
  ];

  public dishObject: any;

  constructor(
    private menuService: MenuService,
    private route: ActivatedRoute
  ) { }

  showPage: any;

  ngOnInit() {
    this.getDish();

    this.showPage = document.querySelector('#show-page');
    setTimeout(() => {
      this.showPage.classList.remove('hidden');
    }, 500);
  }

  getDish() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.dishObject = this.menuService.getDish(id);
  }

  ngAfterViewInit() {
    Scrollbar.init(document.getElementById('menu-item'));

    const scrollStyle = `
    [data-scrollbar] {
      display: block;
      position: relative;
    }
    
    .scroll-content {
      -webkit-transform: translate3d(0, 0, 0);
              transform: translate3d(0, 0, 0);
    }
    
    .scrollbar-track {
      position: absolute;
      opacity: .2;
      z-index: 999999;
      background:  rgba(18,18,18,0.4);
      -webkit-user-select: none;
         -moz-user-select: none;
          -ms-user-select: none;
              user-select: none;
      -webkit-transition: opacity 0.5s 0.5s ease-out;
              transition: opacity 0.5s 0.5s ease-out;
    }
    .scrollbar-track.show,
    .scrollbar-track:hover {
      opacity: 1;
      -webkit-transition-delay: 0s;
              transition-delay: 0s;
    }
    
    .scrollbar-track-x {
      bottom: 0;
      left: 0;
      width: 100%;
      height: 8px;
    }
    .scrollbar-track-y {
      top: 0;
      right: 0;
      width: 8px;
      height: 100%;
    }
    .scrollbar-thumb {
      position: absolute;
      top: 0;
      left: 0;
      width: 8px;
      height: 8px;
      background: rgba(0, 0, 0, .5);
      border-radius: 4px;
      background: rgb(252, 251, 3); 
    }`;

    document.getElementById('smooth-scrollbar-style').innerHTML = scrollStyle;
  }

}
