import { Directive, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[MenuHover]'
})
export class MenuHoverDirective {

  @HostBinding('class.menu-hover') isHovered: boolean = false;

  @HostListener('mouseenter') onmouseenter() {
    this.isHovered = true;
  }

  @HostListener('mouseleave') onmouseleave() {
    this.isHovered = false;
  }

}
