import { Component, OnInit, AfterViewInit } from '@angular/core';
import Scrollbar from 'smooth-scrollbar';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: [
    './blog.component.css', 
    './blog.media.component.css'
  ]
})
export class BlogComponent implements OnInit, AfterViewInit {

  newsData = [
    {
      description: `Давно выяснено, что при оценке дизайна и композиции читаемый текст 
      мешает сосредоточиться. Lorem Ipsum используют`
    },
    {
      description: `Давно выяснено, что при оценке дизайна и композиции читаемый текст 
      мешает сосредоточиться. Lorem Ipsum используют`
    },
    {
      description: `Давно выяснено, что при оценке дизайна и композиции читаемый текст 
      мешает сосредоточиться. Lorem Ipsum используют`
    },
    {
      description: `Давно выяснено, что при оценке дизайна и композиции читаемый текст 
      мешает сосредоточиться. Lorem Ipsum используют`
    }
  ]

  showPage: any;

  constructor() { }

  ngOnInit() {
    this.showPage = document.querySelector('#show-page');
    setTimeout(() => {
      this.showPage.classList.remove('hidden');
    }, 500);
  }

  ngAfterViewInit() {

    Scrollbar.init(document.getElementById('blog'));

    const scrollStyle = `
    [data-scrollbar] {
      display: block;
      position: relative;
    }
    
    .scroll-content {
      -webkit-transform: translate3d(0, 0, 0);
              transform: translate3d(0, 0, 0);
    }
    
    .scrollbar-track {
      position: absolute;
      opacity: .2;
      z-index: 999999;
      background:  rgba(18,18,18,0.4);
      -webkit-user-select: none;
         -moz-user-select: none;
          -ms-user-select: none;
              user-select: none;
      -webkit-transition: opacity 0.5s 0.5s ease-out;
              transition: opacity 0.5s 0.5s ease-out;
    }
    .scrollbar-track.show,
    .scrollbar-track:hover {
      opacity: 1;
      -webkit-transition-delay: 0s;
              transition-delay: 0s;
    }
    
    .scrollbar-track-x {
      bottom: 0;
      left: 0;
      width: 100%;
      height: 8px;
    }
    .scrollbar-track-y {
      top: 0;
      right: 0;
      width: 8px;
      height: 100%;
    }
    .scrollbar-thumb {
      position: absolute;
      top: 0;
      left: 0;
      width: 8px;
      height: 8px;
      background: rgba(0, 0, 0, .5);
      border-radius: 4px;
      background: rgb(252, 251, 3); 
    }`;

    document.getElementById('smooth-scrollbar-style').innerHTML = scrollStyle;

  }
  
}
