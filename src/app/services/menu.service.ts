import { Injectable } from '@angular/core';

@Injectable()
export class MenuService {

  dishesData = [
    {
      image: '../../assets/images/dish1.png',
      category: 'Основное меню',
      id: 1
    },
    {
      image: '../../assets/images/dish2.png',
      category: 'Event меню',
      id: 2
    },
    {
      image: '../../assets/images/dish3.png',
      category: 'Винная карта',
      id: 3
    },
  ];

  constructor() { }

  public getDishes() {
    return this.dishesData;
  }

  public getDish(id: number) {
    for (let i = 0; i < this.dishesData.length; i++) {
      if (this.dishesData[i].id === id) {
        return this.dishesData[i];
      }
    }
  }

}
