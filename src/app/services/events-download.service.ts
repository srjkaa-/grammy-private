import { Injectable } from '@angular/core';
import { Events } from '../interfaces/events';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class EventsDownloadService {
  cachedValues: Array<{
    [query: string]: Events
  }> = [];

  constructor(private http: HttpClient) { }

  eventsDownload = (query: string): Promise<Events> => {
    let promise = new Promise<Events>((resolve, reject) => {
      if (this.cachedValues[query]) {
        resolve(this.cachedValues[query]);
      } else {
        this.http.get(query)
          .toPromise()
          .then( response => {
            resolve(response as Events);
          }, error => {
            reject(error);
          })
      }
    });

    return promise;
  }

}
