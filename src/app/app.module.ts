import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { EventsComponent } from './pages/events/events.component';
import { MenuComponent } from './pages/menu/menu.component';
import { BlogComponent } from './pages/blog/blog.component';
import { GalleryComponent } from './pages/gallery/gallery.component';
import { AboutComponent } from './pages/about/about.component';
import { ContactsComponent } from './pages/contacts/contacts.component';
import { GreetComponent } from './shared/greet/greet.component';

import { EventsDownloadService } from './services/events-download.service';
import { MenuService } from './services/menu.service';

import { MenuHoverDirective } from './directives/menu-hover.directive';
import { MenuItemComponent } from './shared/menu-item/menu-item/menu-item.component';

const ROUTES: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'blog',
    component: BlogComponent
  },
  {
    path: 'contacts',
    component: ContactsComponent
  },
  {
    path: 'events',
    component: EventsComponent
  },
  {
    path: 'gallery',
    component: GalleryComponent
  },
  {
    path: 'menu',
    component: MenuComponent
  },
  {
    path: 'menu/:id',
    component: MenuItemComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    EventsComponent,
    MenuComponent,
    BlogComponent,
    GalleryComponent,
    AboutComponent,
    ContactsComponent,
    GreetComponent,
    MenuHoverDirective,
    MenuItemComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [
    EventsDownloadService,
    MenuService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }